def split_list(lst):
    unique = []
    duplicates = []
    for item in lst:
        if lst.count(item) == 1:
            unique.append(item)
        else:
            duplicates.append(item)
    return unique, duplicates

# Example usage
my_list = [1, 2, 3, 4, 4, 5, 6, 6, 7]
unique_list, duplicates_list = split_list(my_list)
print("Unique elements:", unique_list)
print("Duplicate elements:", duplicates_list)
