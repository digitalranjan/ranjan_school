# -* coding: utf-8 -*

from odoo import models, fields, api


class StudentDataFetch(models.Model):
    _name = "student.fetch"
    _description = 'Student Data Fetch'

    name = fields.Char(string="First Name")
    m_name = fields.Char(string="Middle Name")
    l_name = fields.Char(string="Last Name")


    def create_student(self):
        print("Hello I'm Student")
        vals = {
            'name': self.name,
            'm_name': self.m_name,
            'l_name': self.l_name
        }
        self.env['res.partner'].create(vals)
        return True