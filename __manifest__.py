# -*- coding: utf-8 -*-

{
    'name': 'Ranjan School',
    'version': '16.0',
    'summary': 'Demo',
    'sequence': 10,
    'description': """School Management System""",
    'category': 'Education',
    'website': 'https://www.oddd.com',
    'depends': ['base', 'stock'],
    'data': [
        'security/ir.model.access.csv',
        'data/ir_sequence_data.xml',
        'data/grade.xml',
        'views/student_profile_view.xml',
        'views/student_grade_view.xml',
        'views/academic_year_view.xml',
        # 'views/student_view.xml',
        'report/report.xml',
        'report/report_template.xml',
        # 'wizard/student_data_fetch_view.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'license': 'LGPL-3',
}
