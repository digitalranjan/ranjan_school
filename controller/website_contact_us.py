from odoo import http
from odoo.http import request

class WebsiteContactUs(http.Controller):

    @http.route('/contactus', type='http', auth='public', website=True)
    def contactus_form(self, **kwargs):
        return request.render('my_module.contactus_form', {})

    @http.route('/contactus/submit', type='http', auth='public', website=True)
    def contactus_submit(self, **kwargs):
        name = kwargs.get('name')
        email = kwargs.get('email')
        phone = kwargs.get('phone')
        message = kwargs.get('message')

        contactus = request.env['website.contactus'].create({
            'name': name,
            'email': email,
            'phone': phone,
            'message': message,
        })

        return request.redirect('/thankyou')




