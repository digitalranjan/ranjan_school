from odoo import models, _

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    def unlink(self):
        for picking in self:
            if picking.state == 'draft' and len(picking.move_ids_without_package) > 3:
                print(":::::::::::::: Items Can Not delete")
                warning = {
                    'title': _('Warning!'),
                    'message': _('This picking contains more than 3 picking lines. Are you sure you want to delete it?'),
                }
                return {'warning': warning}
            else:
                print("::::: item deleted")
        return super(StockPicking, self).unlink()
