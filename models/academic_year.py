# -* coding: utf-8 -*

from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import ValidationError

class AcademicsYear(models.Model):
    _name = 'academics.year'
    _description = "Academics Year"

    name = fields.Char(string="Name")
    start_date = fields.Date(string="Start Date")
    end_date = fields.Date(string="End Date")

    @api.onchange('name')
    def valid_acedemic_name(self):
        for rec in self:
            if rec.name:
                if len(rec.name) != 9:
                    raise ValidationError(
                        _('Enter a valid academic year !!'))
                if len(rec.name) == 9:
                    dt = rec.name
                    x = dt.split("-")
                    print(x)
                    start_date = '01' + "/" + '04' + "/" + x[0]
                    end_date = '31' + "/" + '03' + "/" + x[1]
                    rec.start_date = datetime.strptime(start_date, '%d/%m/%Y').strftime("%Y-%m-%d")
                    rec.end_date = datetime.strptime(end_date, '%d/%m/%Y').strftime("%Y-%m-%d")
