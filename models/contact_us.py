# -* coding: utf-8 -*

from odoo import models, fields

class ContactUs(models.Model):
    _name = 'website.contactus'
    _description = 'Contact Us Form'

    name = fields.Char(string='Name')
    email = fields.Char(string='Email')
    phone = fields.Char(string='Phone')
    message = fields.Text(string='Message')