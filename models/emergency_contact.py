# -*- coding: utf-8 -*-

from odoo import fields, models, api, _


class EmergencyContact(models.Model):
    _name = 'emergency.contact'
    _description = 'Emergency Contact'
    _rec_name = 'name'

    name = fields.Char(string="Name", required=True)
    phone = fields.Char(string="Phone", required=True)
    relation = fields.Char(string="Relation")
    contact_id = fields.Many2one('student.profile', string="Contact")
