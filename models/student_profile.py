# -*- coding: utf-8 -*-

from datetime import datetime
from odoo import models, fields, api, _
import re
from odoo.exceptions import ValidationError


class StudentProfile(models.Model):
    _name = 'student.profile'
    _description = "Student Profile"
    _rec_name = 'name'
    # _inherit = 'res.partner'


    @api.constrains('phone')
    def _check_phone_number(self):
        for record in self:
            if len(self.phone) != 10:
                raise ValidationError("Phone number should be 10 digits.")
            if not record.phone[0] in ['6', '7', '8', '9']:
                raise ValidationError("Phone number should start with a digit between 6 and 9.")

    # @api.constrains('joining_date')
    # def _set_joining_date(self):
    #     for record in self:
    #         # joining_date = self.joining_date
    #         # if joining_date:
    #         # if record.academic_id:
    #         academic_year_start_date = record.academic_id.start_date if record.academic_id and record.academic_id.start_date else False
    #         academic_year_end_date = record.academic_id.end_date if record.academic_id and record.academic_id.end_date else False
    #         if academic_year_start_date == False and academic_year_end_date == False:
    #             raise ValidationError(
    #                 _('Start date and end dates are not there in academic year !!'))
    #         elif academic_year_start_date != False and record.joining_date != False and academic_year_end_date != False and academic_year_start_date <= record.joining_date <= academic_year_end_date:
    #             pass
    #         else:
    #             raise ValidationError(
    #                 _('Joining date should be within the current academic year start and end date.'))

    name = fields.Char(string=" Full Name", compute='_compute_full_name', store=1)
    f_name = fields.Char(string="First Name", required=1)
    m_name = fields.Char(string="Middle Name")
    l_name = fields.Char(string="Last Name", required=1)
    stu_id = fields.Char(string="Student ID", readonly=1, index=True, copy=False, default='New')
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
    ], string="Gender", default='male', required=True)
    image = fields.Binary()
    dob = fields.Date(string="Date of Birth", required=1)
    age = fields.Integer(string="Age")
    address = fields.Char(string="Address")
    phone = fields.Char(string="Phone")
    email = fields.Char(string="Email", required=True)
    academic_year_id = fields.Many2one('academics.year', string="Academic Year")
    # contact_ids = fields.One2many('emergency.contact', 'contact_id', string='Emergency Contact')
    grade_id = fields.Many2one('student.grade', string="Grade")
    class_ids = fields.One2many('profile', 'relation_id', string="Class")
    enrolling_status = fields.Selection([('drafted', 'Drafted,'),
                               ('accepted,', 'Accepted,'),
                               ('admitted,', 'Admitted,'),
                               ('graduated,', 'Graduated,'), ], default='drafted')
    join_grade = fields.Char(string='Joining Grade')
    reg_no = fields.Char(string='Registration No', required=True)
    joining_date = fields.Date(string='Joining Date')
    academic_id = fields.Many2one('academics.year', string="Academic Year")
    year_entry = fields.Selection(selection=lambda self: self._compute_year(), string="Year of Entry")
    graduation_year = fields.Selection(selection='_compute_graduation_years', string='Graduation Year')

    @api.onchange('grade_id', 'joining_date')
    def _get_graduation_year(self):
        if self.joining_date and self.grade_id:
            print("Joining Date  Year::::::",self.joining_date.year)
            print("Grade::::::",self.grade_id)
            excepted_graduation_year = (self.joining_date.year+12)-self.grade_id.level
            print("Expected Year ::::::::", excepted_graduation_year)
            self.update({'graduation_year': str(excepted_graduation_year)})

    def _compute_graduation_years(self):
        current_year = datetime.now().year
        end_year = current_year + 12
        graduation_years = [(str(year), str(year)) for year in range(2022, end_year + 1)]
        # print(graduation_years)
        return graduation_years

    def _compute_year(self):
        year_of_entry = [(f"{year}-{year + 1}", f"{year}-{year + 1}") for year in range(2010, (datetime.now().year) + 1)]
        # print(year_of_entry)
        return year_of_entry


    @api.model
    def create(self, vals):
        print(".........", self.env['ir.sequence'])
        vals['stu_id'] = self.env['ir.sequence'].next_by_code('student.profile')
        return super(StudentProfile, self).create(vals)

    @api.depends('f_name', 'm_name', 'l_name')
    def _compute_full_name(self):
        for record in self:
            record.name = '%s %s %s' % (record.f_name or '', record.m_name or '', record.l_name or '')

    @api.onchange('email')
    def valid_email(self):
        if self.email:
            match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', self.email)
            if match == None:
                raise ValidationError(_('Please Enter Valid Email Only..!'))

    @api.constrains('dob')
    def _check_birthdate(self):
        today = datetime.today()
        for record in self:
            if record.dob:
                age = today.year - record.dob.year - (
                        (today.month, today.day) < (record.dob.month, record.dob.day))
                if age < 4 or age > 16:
                    raise ValidationError('Age must be between 4 and 16 years')

class Profile(models.Model):
    _name = 'profile'
    _description = "Profile"

    name = fields.Char(string="name")
    phone = fields.Char(string="phone", required=1)
    relation_id = fields.Many2one('student.profile')
