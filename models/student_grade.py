# -* coding: utf-8 -*

from odoo import models, fields, api, _


class StudentGrade(models.Model):
    _name = 'student.grade'
    _description = "Student Grade"

    name = fields.Char(string="Name")
    level = fields.Integer(string="Level")

