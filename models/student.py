# -*- coding: utf-8 -*-

from odoo import fields, models, api
from datetime import datetime

class Student(models.Model):
    _inherit = 'res.partner'

    is_student = fields.Boolean(string="Is a student", default=1)
    f_name = fields.Char(string="First Name")
    m_name = fields.Char(string="Middle Name")
    l_name = fields.Char(string="Last Name")
    name = fields.Char(string="Name")
    fullname = fields.Char(string="Full Name")
    # fname = fields.Char(string="Name", compute='_compute_full_name', store=1)
    grade_id = fields.Many2one('student.grade', string="Grade")
    graduation_year = fields.Selection(selection='_compute_graduation_years', string='Graduation Year')

    @api.onchange('name')
    def _get_name(self):
        print(":::::object",self)
        print(":::::name",self.name)
        print(":::::First Name",self.f_name)
        print(":::::Full Name",self.fullname)
        for record in self:
            record.fullname = '%s %s %s' % (record.f_name or '', record.m_name or '', record.l_name or '')
            print("::::::::record",record.fullname)


    # @api.depends('f_name', 'm_name', 'l_name')
    # def _compute_full_name(self):
    #     for record in self:
    #         # print(":::::::", self.fname)
    #
    #         record.name = '%s %s %s' % (record.f_name or '', record.m_name or '', record.l_name or '')

    def _compute_graduation_years(self):
        current_year = datetime.now().year
        end_year = current_year + 12
        graduation_years = [(str(year), str(year)) for year in range(2022, end_year + 1)]
        # print(graduation_years)
        return graduation_years

    # add other fields as per your requirement for student profile form

    @api.onchange('grade_id')
    def _get_graduation_year(self):
        print("self:::::::",self)
        print("Grade:::::::",self.grade_id)
        print("graduation_years:::::::",self.graduation_year)
        for rec in self:
            print('\n---rec::::::', rec)
            if rec.grade_id:
                starting_grade = 1
                graduation_grade = 12
                ex_yr = graduation_grade - int(rec.grade_id)
                current_year = datetime.now().year
                rec.graduation_year = self.graduation_year
                print("Current Year::::::::::::",current_year, 'Grade is', rec.grade_id)
                print("Expected Year::::::::::::", ex_yr + current_year)

